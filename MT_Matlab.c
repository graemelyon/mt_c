/********************************************************************
* MT_Matlab.c
*
* Open multiple instances of MATLAB and run multi-threaded
*
* Graeme Lyon
* April 2014
/*******************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <omp.h>

#define NUM_THREADS 6
#define N_TOTAL 1054
#define CHUNK_LEN 5

//prototype
char* CommaSeparatedListOfIntegers(const int N_from, const int N_to);

int main( int argc, char *argv[] )
{
	int i;
	int s = 3;
	int tc = 4;

#pragma omp parallel for num_threads(NUM_THREADS)
	for(i=1; i<=N_TOTAL-CHUNK_LEN; i+=CHUNK_LEN) {
		char cmd[2000];
		char* list;

		//Generate list
		list = CommaSeparatedListOfIntegers(i, i+CHUNK_LEN-1);

		sprintf(cmd,"\"C:/Program Files/MATLAB/R2012b/bin/matlab.exe\" \
					-nodisplay -minimize -nosplash -nodesktop -wait -r \
					cd('R:/Graeme/SC');RUN_SmartCuff_OMP('%s','%d','%d');exit;", list, tc, s);
		printf("ID:%d\tComplete\n", omp_get_thread_num() );
		system(cmd);
	}

	return 0;
}


char* CommaSeparatedListOfIntegers(const int N_from, const int N_to)
{
	int i,maxLen;
	char* result;
	char* p;

	maxLen = (int) (log10(N_to) + 1);
	result = malloc(N_to*(maxLen+1));
	p = result;
	for (i=N_from; i<=N_to; i++)
	{
		char num[16];
		size_t len = sprintf(num, "%d,", i);
		memcpy(p, num, len);
		p += len;
	}
	*(p-1) = '\0'; //end string

	return result;
}

//EFO